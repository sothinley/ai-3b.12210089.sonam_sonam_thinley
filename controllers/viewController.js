const path = require('path')

/* LOG IN PAGE */
exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'login.html'))
}

/* SIGN UP PAGE */
exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'signup.html'))
}

/* HOME PAGE */
exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'dashboard.html'))
}

/* Dashboard */
exports.getDashBaord = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'dashboard2.html'))
}

/* Profile Page */
exports.getProfile = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'userProfile.html'))
}

/* Admin Dashboard Page */
exports.getAdminDashboard = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'admindashboard.html'))
}

/* Admin Users Page */
exports.getUsers = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'users.html'))
}

/* Admin Feedbacks Page */
exports.getFeedbacks = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'adminfeedback.html'))
}

/* Admin Bookings Page */
exports.getBookings = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'adminbooking.html'))
}