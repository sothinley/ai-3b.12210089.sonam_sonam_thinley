const feedback = require('./../models/feedbackModels')

exports.getAllfeedbacks = async (req, res, next) => {
    try{
        const users = await feedback.find()
        res.status(200).json({data: users, status: 'success'})
    }catch (err) {
        res.status(500).json({error: err.message});
    }
}

exports.createfeedback = async (req, res) => {
    try{
        const user = await feedback.create(req.body);
        res.json({ data: user, status: "success"});
    }catch (err) {
        res.status(500).json({ error: err.message});
    }
}

exports.getfeedback = async (req, res) => {
    try{
        const user = await feedback.findById(req.params.id);
        res.json({ data: user, status: "success"});
    } catch (err) {
        res.status(500).json({ error: err.message});
    }
}

exports.updatefeedback = async(req, res) => {
    try{
        const user = await feedback.findByIdAndUpdate(req.params.id, req.body);
        res.json({ data: user, status: "success"});
    } catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.deletefeedback = async(req, res) => {
    try{
        const user = await feedback.findByIdAndDelete(req.params.id);
        res.json({data: user,status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}
