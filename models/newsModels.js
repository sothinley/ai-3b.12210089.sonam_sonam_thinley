const mongoose = require('mongoose')
const validator = require('validator')

const bookingFeedSchema = new mongoose.Schema({
    carmodal: {
        type: String,
        required: [true, 'A name should be unique'],
    },
    carnumber: {
        type: String,
        required: [true, 'A number should be unique'],
    },
    date: {
        type: Date,
        default: Date.now(),
    },
    time: {
        type: String,
        required: true,
    },
    service: {
        type: String,
        required: [true, 'A service should be unique'],
    },
    // SME: Subject Matter Experts
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'Booking must belong to a Users.']
    }
})

bookingFeedSchema.pre(/^find/, function(next){
    this.populate({
        path: 'user',
        select: 'name'
    })
    next()
})

const newsFeed = mongoose.model('NewsFeeds', bookingFeedSchema)
module.exports = newsFeed