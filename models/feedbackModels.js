const mongoose = require('mongoose')
const validator = require('validator')

const feedbackSchema = new mongoose.Schema({
    feedback: {
        type: String,
        required: [true, 'A feedback should be unique'],
    },
    date: {
        type: Date,
        default: Date.now(),
    },
    time: {
        type: String,
        required: true,
    }
})

const feedback = mongoose.model('feedback', feedbackSchema)
module.exports = feedback