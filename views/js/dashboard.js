import { showAlert } from './alert.js'

// Logging Out
const logout = async () => {
    try {
        const res = await axios({
            method: 'GET',
            url: 'http://localhost:4001/api/v1/users/logout',
        })
        if (res.data.status === 'success') {
            location.assign("/")
        }
    } catch (err) {
        showAlert('error', 'Error logging out! Try again.')
    }
}

var obj
if (document.cookie) {
    obj = JSON.parse(document.cookie.substring(6));
} else {
    obj = JSON.parse('{}');
}

var el = document.querySelector('.nav')
var form = document.querySelector('#toadd1')
var form2 = document.querySelector('#toadd2')
if (obj._id) {
    el.innerHTML +=
        '<div class="dropdown"><button class="dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false"><img src="../images/users/' + obj.photo + '" width="20px" height="20px"></button><ul class="dropdown-menu"><li><a class="dropdown-item" href="/me">Edit Profile</a></li><li><a class="dropdown-item" id="logout">Logout</a></li></ul></div>'

    form.innerHTML = '<form id="form_booking"><table class="table-with-background"><tr><td><label for="carModel">Car Model:</label><input type="text" id="carModel" name="carModel" required /><br /><br /><label for="date">Date:</label><input type="date" id="date" name="date" required /><br /><br /></td><td><label for="carNumber">Car Number:</label><input type="text" id="carNumber" name="carNumber" required /><br /><br /><label for="time">Time:</label><input type="time" id="time" name="time" required /><br /><br /></td></tr><tr><td colspan="2"><label for="services">Services that you need:</label><select id="services" name="services" required><option value="oil-change">Oil Change</option><option value="brake-service">Brake Service</option><option value="tire-rotation">Tire Rotation</option><option value="engine-tune-up">Engine Tune-Up</option><option value="transmission-service">Transmission Service</option><option value="coolant-flush">Coolant Flush</option><option value="battery-replacement">Battery Replacement</option><option value="air-filter-replacement">Air Filter Replacement</option><option value="wheel-alignment">Wheel Alignment</option><option value="exhaust-system-repair">Exhaust System Repair</option><option value="suspension-repair">Suspension Repair</option></select><br /><br /></td></tr><tr><td colspan="2"><input type="submit" id="submitBtn" value="Submit"/></td></tr></table></form>'

    form2.innerHTML += '<form id="form_feedback"><label for="feedbackdescription">Your Feedback:</label><textarea id="feedbackdescription" name="feedbackdescription" rows="4" cols="50" required></textarea><br /><input type="submit" value="Submit" /></form>'

    var doc = document.querySelector('#logout')
    doc.addEventListener('click', (e) => logout())

    var doc2 = document.querySelector('#form_booking')
    doc2.addEventListener('submit', (e) => handleFormSubmission(e));

    var doc3 = document.querySelector('#form_feedback')
    doc3.addEventListener('submit', (e) => handleFeedback(e));
} else {
    location.assign("/")
}


async function handleFormSubmission(event) {
    // Prevent the form from being submitted normally
    event.preventDefault();
    const selectedService = document.querySelector('select[name=services]').value;

    console.log(selectedService)

    // Create a new object from the form
    let jsonObject = {
        carmodal: document.querySelector('#carModel').value,
        carnumber: document.querySelector('#carNumber').value,
        date: document.querySelector('#date').value,
        time: document.querySelector('#time').value,
        service: document.querySelector('select[name=services]').value,
        user: obj._id
    };
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://localhost:4001/api/v1/news',
            data: jsonObject
        });

        if (res.data.status === 'success') {
            showAlert('success', 'Booking Created successfully');
        }
    } catch (error) {
        let message =
            typeof error.response !== 'undefined'
                ? error.response.data.message
                : error.message;

        showAlert('error', 'Error: Booking not created', message);
    }
}
async function handleFeedback(event) {
    // Prevent the form from being submitted normally
    event.preventDefault();
    var now = new Date();

    // Get current date in string format
    var dateStr = now.toDateString(); // e.g., "Tue Mar 02 2023"

    // Get current time in string format
    var timeStr = now.toTimeString().split(' ')[0];

    // Create a new object from the form
    let jsonObject = {
        feedback: document.querySelector('#feedbackdescription').value,
        date: dateStr,
        time: timeStr,
    };

    alert(jsonObject.feedback)
    alert(jsonObject.date)
    alert(jsonObject.time)
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://localhost:4001/api/v1/feedback',
            data: jsonObject
        });

        if (res.data.status === 'success') {
            showAlert('success', 'feedback submitted successfully');
        }
    } catch (error) {
        let message =
            typeof error.response !== 'undefined'
                ? error.response.data.message
                : error.message;

        showAlert('error', 'Error: feedback not submitted', message);
    }
}