import { showAlert } from './alert.js'

const logout = async () => {
    try {
        const res = await axios({
            method: 'GET',
            url: 'http://localhost:4001/api/v1/users/logout',
        })
        if (res.data.status === 'success') {
            showAlert('success', 'Logging Out')
            location.assign("/")
        }
    } catch (err) {
        showAlert('error', 'Error logging out! Try again.')
    }
}

var doc = document.querySelector('#logout')
doc.addEventListener('click', (e) => logout())
