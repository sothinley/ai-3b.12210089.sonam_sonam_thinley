import { showAlert } from './alert.js'

const allNews = async () => {
    try {
        const res = await axios({
            method: 'GET',
            url: 'http://localhost:4001/api/v1/feedback',
        })
        displayNews(res.data)
    } catch (err) {
        console.log(err)
    }
}
allNews()

const displayNews = (news) => {
    var arr = news.data
    for (let i = 0; i < arr.length; i++) {
        var tableBody = document.getElementById('table_body');
        var row = tableBody.insertRow(-1);
        const element = arr[i]
        row.insertCell(0).innerHTML = i + 1; // Assuming this is a serial number
        row.insertCell(1).innerHTML = '' + element.feedback
        row.insertCell(2).innerHTML = '' + element.date
        row.insertCell(3).innerHTML = '' + element.time
        var deleteButton = document.createElement('button');
        deleteButton.textContent = 'Delete';
        deleteButton.className = 'btn btn-danger';
        deleteButton.addEventListener('click', function () {
            deleteNews(element._id);
            tableBody.deleteRow(row.rowIndex);
        });
        row.insertCell(4).appendChild(deleteButton);
    }
}

const deleteNews = async (id) => {
    // Ask for confirmation before deleting
    var confirmation = confirm('Are you sure you want to delete this news?');
    if (confirmation) {
        try {
            const res = await axios({
                method: 'DELETE',
                url: `http://localhost:4001/api/v1/feedback/${id}`,
            })
            if (res.data.status === 'success') {
                showAlert('success', 'Deleted a user')
                location.reload()
            }
        } catch (err) {
            showAlert('error', 'Error Deleting user! Try again.')
        }

    } else {
        showAlert('error', 'Cancelled Deleting')
    }
}

