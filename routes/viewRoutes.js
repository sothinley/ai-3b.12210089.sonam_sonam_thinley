const express = require('express')
const router = express.Router()
const viewsController = require('./../controllers/viewController')
const authController = require('./../controllers/authController')

router.get('/', viewsController.getHome)
router.get('/login', viewsController.getLoginForm)
router.get('/signup', viewsController.getSignupForm)
router.get('/me',authController.protect,viewsController.getProfile)
router.get('/dashboard',authController.protect, viewsController.getDashBaord)

/* Routes for admin dashboard */

router.get('/admindashboard',authController.protect, authController.restrictTo('admin'),viewsController.getAdminDashboard)
router.get('/feedbacks',authController.protect, authController.restrictTo('admin'),viewsController.getFeedbacks)
router.get('/bookings',authController.protect, authController.restrictTo('admin'),viewsController.getBookings)
router.get('/users',authController.protect, authController.restrictTo('admin'),viewsController.getUsers)

module.exports = router