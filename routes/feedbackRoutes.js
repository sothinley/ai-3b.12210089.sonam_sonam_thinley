const express = require('express')
const feedbackController = require('./../controllers/feedBackController')
const router = express.Router()

router
    .route('/')
    .get(feedbackController.getAllfeedbacks)
    .post(feedbackController.createfeedback)

router
    .route('/:id')
    .get(feedbackController.getfeedback)
    .patch(feedbackController.updatefeedback)
    .delete(feedbackController.deletefeedback)

module.exports = router